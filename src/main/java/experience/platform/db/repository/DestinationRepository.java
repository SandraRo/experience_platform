package experience.platform.db.repository;

import experience.platform.db.model.Destination;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DestinationRepository extends CrudRepository<Destination, Integer> {
    public Destination findByName(String name);
}
