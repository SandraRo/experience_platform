package experience.platform.db.repository;

import experience.platform.db.model.ReservedTour;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservedTourRepository extends CrudRepository<ReservedTour,Integer> {
}
