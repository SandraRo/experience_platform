package experience.platform.db.repository;

import experience.platform.db.model.Local;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocalRepository extends CrudRepository<Local, Integer> {

    public Local findByUsernameAndPassword(String username, String password);

    public Local findByUsername(String username);
}
