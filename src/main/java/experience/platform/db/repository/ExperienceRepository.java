package experience.platform.db.repository;

import experience.platform.db.model.Experience;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExperienceRepository extends CrudRepository<Experience, Integer> {
    public Experience findByName(String name);
}
