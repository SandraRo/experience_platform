package experience.platform.db.repository;

import experience.platform.db.model.Local;
import experience.platform.db.model.Tour;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TourRepository extends CrudRepository<Tour, Integer> {
    public List<Tour> findByLocal(Local local);

    public Tour findByName(String name);
}
