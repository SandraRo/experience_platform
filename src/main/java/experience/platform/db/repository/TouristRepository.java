package experience.platform.db.repository;

import experience.platform.db.model.Tourist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TouristRepository extends CrudRepository<Tourist, Integer> {
    public Tourist findByUsernameAndPassword(String username, String password);

    public Tourist findByUsername(String username);
}
