package experience.platform.db.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "dozivljaj", schema = "experience_platform", catalog = "")
public class Experience {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private int id;
    @Basic
    @Column(name = "naziv", length = 50)
    private String name;
    @Basic
    @Column(name = "opis", nullable = false, length = 200)
    private String description;
    @Basic
    @Column(name = "cena", nullable = false, precision = 0)
    private double price;
    @ManyToOne
    @JoinColumn(name = "destinacija", referencedColumnName = "id", nullable = false)
    private Destination destination;
    @ManyToOne
    @JoinColumn(name = "tura", referencedColumnName = "id", nullable = false)
    private Tour tour;

    public Experience(String name, String description, double price, Destination destination, Tour tour) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.destination = destination;
        this.tour = tour;
    }

    public Experience() {
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String naziv) {
        this.name = naziv;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String opis) {
        this.description = opis;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double cena) {
        this.price = cena;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Experience that = (Experience) o;
        return id == that.id && Double.compare(that.price, price) == 0 && destination == that.destination && tour == that.tour && Objects.equals(name, that.name) && Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, destination, tour);
    }

    @Override
    public String toString() {
        return "Experience{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", destination=" + destination +
                '}';
    }
}
