package experience.platform.db.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "turista", schema = "experience_platform", catalog = "")
public class Tourist {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private int id;
    @Basic
    @Column(name = "ime", nullable = false, length = 25)
    private String name;
    @Basic
    @Column(name = "prezime", nullable = false, length = 25)
    private String surname;
    @Basic
    @Column(name = "korisnicko_ime", nullable = false, length = 25)
    private String username;
    @Basic
    @Column(name = "lozinka", nullable = false, length = 25)
    private String password;

    public Tourist(String name, String surname, String username, String password) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
    }

    public Tourist() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String ime) {
        this.name = ime;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String prezime) {
        this.surname = prezime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String korisnickoIme) {
        this.username = korisnickoIme;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String lozinka) {
        this.password = lozinka;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tourist that = (Tourist) o;
        return id == that.id && Objects.equals(name, that.name) && Objects.equals(surname, that.surname) && Objects.equals(username, that.username) && Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, username, password);
    }

    @Override
    public String toString() {
        return "Tourist{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
