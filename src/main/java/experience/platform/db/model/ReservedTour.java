package experience.platform.db.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "rezervisana_tura", schema = "experience_platform", catalog = "")
public class ReservedTour {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private int id;
    @Basic
    @Column(name = "datum", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
    @OneToOne
    @JoinColumn(name = "tura", referencedColumnName = "id", nullable = false)
    private Tour tour;
    @OneToOne
    @JoinColumn(name = "turista", referencedColumnName = "id", nullable = false)
    private Tourist tourist;

    public ReservedTour() {
    }

    public ReservedTour(LocalDate date, Tour tour, Tourist tourist) {
        this.date = date;
        this.tour = tour;
        this.tourist = tourist;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate datum) {
        this.date = datum;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public Tourist getTourist() {
        return tourist;
    }

    public void setTourist(Tourist tourist) {
        this.tourist = tourist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservedTour that = (ReservedTour) o;
        return id == that.id && tour == that.tour && tourist == that.tourist && Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, tour, tourist);
    }
}
