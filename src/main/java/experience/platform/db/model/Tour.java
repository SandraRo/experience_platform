package experience.platform.db.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tura", schema = "experience_platform", catalog = "")
public class Tour {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private int id;
    @Basic
    @Column(name = "naziv", nullable = false, length = 50)
    private String name;
    @Basic
    @Column(name = "ukupna_cena", precision = 0)
    private double price;
    @ManyToOne
    @JoinColumn(name = "mestanin", referencedColumnName = "id", nullable = false)
    private Local local;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "tour")
    private Collection<Experience> experiences;

    public Tour(String name, Local local) {
        this.name = name;
        this.local = local;
    }

    public Tour() {
    }

    public Collection<Experience> getExperiences() {
        return experiences;
    }

    public void setExperiences(Collection<Experience> experiences) {
        this.experiences = experiences;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String naziv) {
        this.name = naziv;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double ukupnaCena) {
        this.price = ukupnaCena;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tour that = (Tour) o;
        return id == that.id && Double.compare(that.price, price) == 0 && local == that.local && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, local);
    }

    @Override
    public String toString() {
        return "Tour{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", local=" + local +
                ", experiences=" + experiences +
                '}';
    }
}
