package experience.platform.service;

import experience.platform.db.model.Local;
import experience.platform.db.model.Tourist;
import experience.platform.db.repository.LocalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocalService {

    @Autowired
    private LocalRepository localRepository;

    public void save(Local local) {
        localRepository.save(local);
    }

    public Local findByUsernameAndPassword(String username, String password) {
        return localRepository.findByUsernameAndPassword(username, password);
    }

    public Local findByUsername(String username) {
        return localRepository.findByUsername(username);
    }

    public Local findById(int localId) {
        return localRepository.findById(localId).orElseThrow(() -> new IllegalArgumentException("Invalid local id:" + localId));
    }
}
