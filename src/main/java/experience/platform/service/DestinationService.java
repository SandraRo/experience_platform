package experience.platform.service;

import experience.platform.db.model.Destination;
import experience.platform.db.repository.DestinationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DestinationService {

    @Autowired
    DestinationRepository destinationRepository;

    public List<Destination> findAll() {
        return (List<Destination>) destinationRepository.findAll();
    }

    public Destination findById(int destinationId) {
        return destinationRepository.findById(destinationId).orElseThrow(() -> new IllegalArgumentException("Invalid destination id:" + destinationId));
    }

    public Destination findByName(String name) {
        return destinationRepository.findByName(name);
    }

    public void save(Destination destination){
        destinationRepository.save(destination);
    }
}
