package experience.platform.service;

import experience.platform.db.model.Tourist;
import experience.platform.db.repository.TouristRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TouristService {

    @Autowired
    private TouristRepository touristRepository;

    public List<Tourist> findAll() {
        return (List<Tourist>) touristRepository.findAll();
    }

    public void save(Tourist tourist) {
        touristRepository.save(tourist);
    }

    public Tourist findById(Integer id) {
        return touristRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid tourist id:" + id));
    }

    public Tourist findByUsername(String username) {
        return touristRepository.findByUsername(username);
    }

    public void delete(Tourist tourist) {
        touristRepository.delete(tourist);
    }

    public Tourist findByUsernameAndPassword(String username, String password) {
        return touristRepository.findByUsernameAndPassword(username, password);
    }

    public String validate(Tourist tourist) {
        String message = "";
        if(findByUsername(tourist.getUsername()) != null){
            message = "Username is taken, try another one";
        }
        return message;
    }

}
