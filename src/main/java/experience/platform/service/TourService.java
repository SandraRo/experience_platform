package experience.platform.service;

import experience.platform.db.model.Destination;
import experience.platform.db.model.Local;
import experience.platform.db.model.Tour;
import experience.platform.db.repository.TourRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Double.sum;

@Service
public class TourService {

    @Autowired
    private TourRepository tourRepository;

    public List<Tour> findAll() {
        return (List<Tour>) tourRepository.findAll();
    }

    public Tour findById(int id) {
        return tourRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid tour id:" + id));
    }

    public List<Tour> findByDestination(Destination destination) {
        List<Tour> allTours = (List<Tour>) tourRepository.findAll();
        List<Tour> destinationTours = new LinkedList<>();
        allTours.forEach((tour) -> {
            if (tour.getLocal().getDestination().equals(destination)) {
                destinationTours.add(tour);
            }
        });
        return destinationTours;
    }

    public List<Tour> findByLocal(Local local) {
        List<Tour> tours = tourRepository.findByLocal(local);
        return tours;
    }

    public void save(Tour tour) {
        tourRepository.save(tour);
    }

    public void delete(Tour tour){
        tourRepository.delete(tour);
    }

    public Tour findbyName(String name) {
        return tourRepository.findByName(name);
    }

    public void setPrice(Tour tour){
        Double sum = tour.getExperiences().stream()
                .map(x -> x.getPrice())
                .reduce((double) 0, Double::sum);
        tour.setPrice(sum);
        tourRepository.save(tour);
        return;
    }
}
