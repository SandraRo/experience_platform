package experience.platform.service;

import experience.platform.db.model.ReservedTour;
import experience.platform.db.repository.ReservedTourRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReservedTourService {

    @Autowired
    private ReservedTourRepository reservedTourRepository;

    public void save(ReservedTour reservedTour) {
        reservedTourRepository.save(reservedTour);
    }
}
