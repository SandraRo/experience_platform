package experience.platform.service;

import experience.platform.db.model.Experience;
import experience.platform.db.repository.ExperienceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExperienceService {

    @Autowired
    private ExperienceRepository experienceRepository;

    public void save(Experience experience) {
        experienceRepository.save(experience);
    }

    public Experience findById(int id){
        return experienceRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid experience id:" + id));
    }

    public void delete(Experience experience) {
        experienceRepository.delete(experience);
    }

    public void deleteById(int experienceId) {
        experienceRepository.deleteById(experienceId);
    }

    public Experience findByName(String name) {
        return experienceRepository.findByName(name);
    }
}
