package experience.platform.api.controller;

import experience.platform.api.model.ExperienceCreationRequest;
import experience.platform.db.model.Destination;
import experience.platform.db.model.Local;
import experience.platform.db.model.Tour;
import experience.platform.db.model.Tourist;
import experience.platform.service.DestinationService;
import experience.platform.service.LocalService;
import experience.platform.service.TourService;
import experience.platform.service.TouristService;
import experience.platform.util.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.List;


@Controller
public class TourController {

    private final TourService tourService;
    private final TouristService touristService;

    private final DestinationService destinationService;

    private final LocalService localService;

    private final Logger logg = LoggerFactory.getLogger(getClass());

    @Autowired
    public TourController(TourService tourService, TouristService touristService, DestinationService destinationService, LocalService localService) {
        this.tourService = tourService;
        this.touristService = touristService;
        this.destinationService = destinationService;
        this.localService = localService;
    }

    @PostMapping("/tour/search/{touristId}")
    public String tourSearch(@PathVariable int touristId, @ModelAttribute("destination") Destination destination, Model model, RedirectAttributes redirectAttributes) {
        Destination dest = destinationService.findByName(destination.getName());
        Tourist tourist = touristService.findById(touristId);
        redirectAttributes.addFlashAttribute("loggedTourist", tourist);
        redirectAttributes.addFlashAttribute("destination", new Destination());
        redirectAttributes.addFlashAttribute("date", LocalDate.now());
        if(dest == null && !destination.getName().isEmpty()){
            redirectAttributes.addFlashAttribute("error", "Destination not available at the moment");
            return "redirect:/tour/search";
        } else if(destination.getName().isEmpty()){
            redirectAttributes.addFlashAttribute("tours", tourService.findAll());
            return "redirect:/tour/search";
        }
        List<Tour> tours = tourService.findByDestination(dest);
        redirectAttributes.addFlashAttribute("tours", tours);
        return "redirect:/tour/search";
    }

    @GetMapping("/tour/search")
    public String tourSearchError(Model model){
        return "tourist-home";
    }

    @GetMapping("/tour/view/{tourId}/{touristId}")
    public String tourView(Model model, @PathVariable int tourId, @PathVariable int touristId) {
        Tour tour = tourService.findById(tourId);
        Tourist tourist = touristService.findById(touristId);
        model.addAttribute("tour", tour);
        model.addAttribute("experiences", tour.getExperiences());
        return "tour-view";
    }

    @PostMapping("/tour/create/{localId}")
    public String tourCreate(Model model, @ModelAttribute("newTour") Tour tour, @PathVariable int localId, RedirectAttributes redirectAttributes) {
        Local local = localService.findById(localId);
        redirectAttributes.addFlashAttribute("tours", tourService.findByLocal(local));
        redirectAttributes.addFlashAttribute("local", local);
        redirectAttributes.addFlashAttribute("destination", new Destination());
        redirectAttributes.addFlashAttribute("newTour", new Tour());
        if (!Validator.isValidTour(tour)) {
            redirectAttributes.addFlashAttribute("error", "Tour name cannot be emty");
            return "redirect:/tour/create/result";
        }
        tour.setLocal(local);
        Tour dbTour = tourService.findbyName(tour.getName());
        if (dbTour == null) {
            tourService.save(tour);
            redirectAttributes.addFlashAttribute("tours", tourService.findByLocal(local));
            redirectAttributes.addFlashAttribute("success", "Tour successfully created");
        } else {
            redirectAttributes.addFlashAttribute("error", "Tour with the same name already exists");
        }
        return "redirect:/tour/create";
    }

    @GetMapping("/tour/create")
    public String tourCreate(Model model) {
        return "local-home";
    }

    @RequestMapping("/tour/edit/{tourId}")
    public String tourEdit(Model model, @PathVariable int tourId) {
        Tour tour = tourService.findById(tourId);
        model.addAttribute("experiences", tour.getExperiences());
        model.addAttribute("tour", tour);
        model.addAttribute("newExperience", new ExperienceCreationRequest());
        return "tour-edit";
    }

    @RequestMapping("/tour/delete/{tourId}/{localId}")
    public String tourDelete(Model model, @PathVariable int tourId, @PathVariable int localId, RedirectAttributes redirectAttributes) {
        Tour tour = tourService.findById(tourId);
        tourService.delete(tour);
        Local local = localService.findById(localId);
        redirectAttributes.addFlashAttribute("local", local);
        redirectAttributes.addFlashAttribute("tours", tourService.findByLocal(local));
        redirectAttributes.addFlashAttribute("newTour", new Tour());
        redirectAttributes.addFlashAttribute("destination", new Destination());
        redirectAttributes.addFlashAttribute("success", "Tour successfully deleted");
        return "redirect:/tour/delete";
    }

    @GetMapping("/tour/delete")
    public String tourdelete(Model model) {
        return "local-home";
    }
}
