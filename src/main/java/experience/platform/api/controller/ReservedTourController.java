package experience.platform.api.controller;

import experience.platform.db.model.Destination;
import experience.platform.db.model.ReservedTour;
import experience.platform.db.model.Tour;
import experience.platform.db.model.Tourist;
import experience.platform.service.ReservedTourService;
import experience.platform.service.TourService;
import experience.platform.service.TouristService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;

@Controller
public class ReservedTourController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final TourService tourService;
    private final TouristService touristService;

    private final ReservedTourService reservedTourService;

    @Autowired
    public ReservedTourController(TourService tourService, TouristService touristService, ReservedTourService reservedTourService) {
        this.tourService = tourService;
        this.touristService = touristService;
        this.reservedTourService = reservedTourService;
    }

    @RequestMapping("/tour/reserve/{tourId}/{touristId}/{date}")
    public String tourReserve(Model model, @PathVariable int tourId, @PathVariable int touristId,
                              @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date, RedirectAttributes redirectAttributes) {
        Tour tour = tourService.findById(tourId);
        Tourist tourist = touristService.findById(touristId);
        if (date.isBefore(LocalDate.now())) {
            model.addAttribute("loggedTourist", tourist);
            redirectAttributes.addFlashAttribute("experiences", tour.getExperiences());
            redirectAttributes.addFlashAttribute("tour", tour);
            redirectAttributes.addFlashAttribute("date", LocalDate.now());
            redirectAttributes.addFlashAttribute("destination", new Destination());
            redirectAttributes.addFlashAttribute("error", "Reservation day must be the day after today");
            return "redirect:/tour/reserve/error";
        }
        ReservedTour reservedTour = new ReservedTour(date, tour, tourist);
        reservedTourService.save(reservedTour);
        redirectAttributes.addFlashAttribute("success", "Tour successfully reserved");
        redirectAttributes.addFlashAttribute("reservedTour", reservedTour);
        return "redirect:/tour/reserve";
    }

    @GetMapping("/tour/reserve")
    public String tourReserve(Model model) {
        return "reserved-tour";
    }

    @GetMapping("/tour/reserve/error")
    public String tourReserveError(Model model) {
        return "tourist-home";
    }
}
