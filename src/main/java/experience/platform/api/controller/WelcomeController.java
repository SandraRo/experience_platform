package experience.platform.api.controller;

import experience.platform.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WelcomeController {

    private final TourService tourService;

    @Autowired
    public WelcomeController(TourService tourService) {
        this.tourService = tourService;
    }

    @GetMapping("/")
    public String showAllTours(Model model) {
        model.addAttribute("tours", tourService.findAll());
        return "index";
    }
}
