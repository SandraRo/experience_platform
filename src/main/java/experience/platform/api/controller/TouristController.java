package experience.platform.api.controller;


import experience.platform.db.model.*;
import experience.platform.service.DestinationService;
import experience.platform.service.TourService;
import experience.platform.service.TouristService;
import experience.platform.util.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;

@Controller
public class TouristController {

    private final TouristService touristService;
    private final TourService tourService;
    private final DestinationService destinationService;

    private final Logger logg = LoggerFactory.getLogger(getClass());

    @Autowired
    public TouristController(TouristService touristService, TourService tourService, DestinationService destinationService) {
        this.touristService = touristService;
        this.tourService = tourService;
        this.destinationService = destinationService;
    }

    @GetMapping("/tourist/signup")
    public String showSignUpForm(Model model) {
        Tourist tourist = new Tourist();
        model.addAttribute(tourist);
        return "add-tourist";
    }

    @PostMapping("/tourist/add")
    public String signup(Model model, @ModelAttribute("tourist") Tourist tourist, RedirectAttributes redirectAttributes) {
        Tourist dbTourist = touristService.findByUsername(tourist.getUsername());
        if (dbTourist != null) {
            redirectAttributes.addFlashAttribute("error", "Username already taken");
            redirectAttributes.addFlashAttribute("tourist", new Tourist());
            return "redirect:/tourist/add/result/error";
        }
        touristService.save(tourist);
        redirectAttributes.addFlashAttribute("tourist", new Tourist());
        redirectAttributes.addFlashAttribute("success", "Tourist account created");
        return "redirect:/tourist/add/result/success";
    }

    @GetMapping("/tourist/add/result/error")
    public String signupError(Model model) {
        return "add-tourist";
    }

    @GetMapping("/tourist/add/result/success")
    public String signupSuccess(Model model) {
        return "login-tourist";
    }


    @GetMapping("/tourist/login")
    public String showLoginForm(Model model) {
        model.addAttribute(new Tourist());
        return "login-tourist";
    }

    @PostMapping("/tourist/login")
    public String login(Model model, @ModelAttribute("tourist") Tourist tourist, RedirectAttributes redirectAttributes) {
        tourist = touristService.findByUsernameAndPassword(tourist.getUsername(), tourist.getPassword());
        if (tourist != null) {
            model.addAttribute("loggedTourist", tourist);
            model.addAttribute("destination", new Destination());
            model.addAttribute("tours", tourService.findAll());
            model.addAttribute("date", LocalDate.now());
            return "tourist-home";
        }
        redirectAttributes.addFlashAttribute("error", "Username or password don't match any user");
        return "redirect:/tourist/login";
    }
}
