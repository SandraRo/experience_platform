package experience.platform.api.controller;

import experience.platform.api.model.ExperienceCreationRequest;
import experience.platform.db.model.Destination;
import experience.platform.db.model.Experience;
import experience.platform.db.model.Tour;
import experience.platform.service.DestinationService;
import experience.platform.service.ExperienceService;
import experience.platform.service.TourService;
import experience.platform.util.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class ExperienceController {

    private final DestinationService destinationService;

    private final TourService tourService;

    private final ExperienceService experienceService;

    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    public ExperienceController(DestinationService destinationService, TourService tourService, ExperienceService experienceService) {
        this.destinationService = destinationService;
        this.tourService = tourService;
        this.experienceService = experienceService;
    }

    @PostMapping("/experience/add/{tourId}")
    public String experienceCreate(Model model, @ModelAttribute("experience") ExperienceCreationRequest experienceCreationRequest,
                                   @PathVariable int tourId, RedirectAttributes redirectAttributes) {
        Tour tour = tourService.findById(tourId);
        Destination destination = destinationService.findByName(experienceCreationRequest.getDestination());
        Experience dbExperience = experienceService.findByName(experienceCreationRequest.getName());
        if(dbExperience != null && dbExperience.getTour().getId() == tourId){
            redirectAttributes.addFlashAttribute("newExperience", dbExperience);
            redirectAttributes.addFlashAttribute("tour", tour);
            redirectAttributes.addFlashAttribute("experiences", tour.getExperiences());
            redirectAttributes.addFlashAttribute("error", "There is already experience with the same name in this tour");
            return "redirect:/experience/add";
        }
        if (destination != null) {
            Experience experience = new Experience(experienceCreationRequest.getName(), experienceCreationRequest.getDescription(),
                    Double.parseDouble(experienceCreationRequest.getPrice()), destination, tour);
            experienceService.save(experience);
            tour = tourService.findById(tourId);
            tourService.setPrice(tour);
            redirectAttributes.addFlashAttribute("tour", tour);
            redirectAttributes.addFlashAttribute("experiences", tour.getExperiences());
            redirectAttributes.addFlashAttribute("success", "Experience successfully created");
            redirectAttributes.addFlashAttribute("newExperience", new ExperienceCreationRequest());
            return "redirect:/experience/add";
        }
        redirectAttributes.addFlashAttribute("error", "Destination not found");
        redirectAttributes.addFlashAttribute("tour", tour);
        redirectAttributes.addFlashAttribute("experiences", tour.getExperiences());
        redirectAttributes.addFlashAttribute("newExperience", new Experience());
        return "redirect:/experience/add";
    }

    @GetMapping("/experience/add")
    public String experienceCreate(Model model) {
        return "tour-edit";
    }

    @RequestMapping("/experience/delete/{experienceId}/{tourId}")
    public String experienceDelete(Model model, @PathVariable int experienceId, @PathVariable int tourId, RedirectAttributes redirectAttributes) {
        experienceService.deleteById(experienceId);
        Tour tour = tourService.findById(tourId);
        redirectAttributes.addFlashAttribute("experiences", tour.getExperiences());
        redirectAttributes.addFlashAttribute("tour", tour);
        redirectAttributes.addFlashAttribute("newExperience", new ExperienceCreationRequest());
        redirectAttributes.addFlashAttribute("success", "Experience successfully deleted");
        return "redirect:/experience/delete";
    }

    @GetMapping("/experience/delete")
    public String experienceDelete(Model model) {
        return "tour-edit";
    }
}

