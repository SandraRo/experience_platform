package experience.platform.api.controller;

import experience.platform.api.model.LocalRegistrationRequest;
import experience.platform.db.model.Destination;
import experience.platform.db.model.Local;
import experience.platform.db.model.Tour;
import experience.platform.service.DestinationService;
import experience.platform.service.LocalService;
import experience.platform.service.TourService;
import experience.platform.util.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;


@Controller
public class LocalController {

    private final LocalService localService;
    private final DestinationService destinationService;
    private final TourService tourService;

    Logger logg = LoggerFactory.getLogger(getClass());

    @Autowired
    public LocalController(LocalService localService, DestinationService destinationService, TourService tourService) {
        this.localService = localService;
        this.destinationService = destinationService;
        this.tourService = tourService;
    }

    @GetMapping("/local/signup")
    public String showSignUpForm(Model model) {
        Local local = new Local();
        model.addAttribute(local);
        model.addAttribute("destinations", destinationService.findAll());
        int destinationId = 0;
        model.addAttribute("destinationId", destinationId);
        return "add-local";
    }

    @PostMapping("/local/add")
    public String signup(Model model, @ModelAttribute("local") LocalRegistrationRequest localRequest, RedirectAttributes redirectAttributes) {
        if(!Validator.isValidLocal(localRequest)){
            redirectAttributes.addFlashAttribute("error", "None of the parameters cannot be empty");
            redirectAttributes.addFlashAttribute("local", new Local());
            return "redirect:/local/add/result/error";
        }
        Destination destination = destinationService.findByName(localRequest.getDestination());
        Local dbLocal = localService.findByUsername(localRequest.getUsername());
        if(destination == null){
            redirectAttributes.addFlashAttribute("error", "Destination doesnt exist");
            redirectAttributes.addFlashAttribute("local", new Local());
            return "redirect:/local/add/result/error";
        } else if(dbLocal != null){
            redirectAttributes.addFlashAttribute("error", "Username already taken");
            redirectAttributes.addFlashAttribute("local", new Local());
            return "redirect:/local/add/result/error";
        }
        Local local = new Local(localRequest.getName(), localRequest.getSurname(), localRequest.getUsername(), localRequest.getPassword(), destination);
        localService.save(local);
        redirectAttributes.addFlashAttribute("local", new Local());
        redirectAttributes.addFlashAttribute("success", "Account for host created");
        return "redirect:/local/add/result/success";
    }

    @GetMapping("/local/add/result/error")
    public String signupError(Model model) {
        return "add-local";
    }

    @GetMapping("/local/add/result/success")
    public String signupSuccess(Model model) {
        return "login-local";
    }


    @GetMapping("/local/login")
    public String showLoginForm(Model model) {
        model.addAttribute(new Local());
        return "login-local";
    }

    @PostMapping("/local/login")
    public String login(Model model, @ModelAttribute("local") Local local, RedirectAttributes redirectAttributes) {
        local = localService.findByUsernameAndPassword(local.getUsername(), local.getPassword());
        if(local != null){
            model.addAttribute("local", local);
            model.addAttribute("tours", tourService.findByLocal(local));
            model.addAttribute("newTour", new Tour());
            model.addAttribute("destination", new Destination());
            return "local-home";
        }
        redirectAttributes.addFlashAttribute("error", "Username or password don't match any user");
        return "redirect:/local/login";
    }

}
