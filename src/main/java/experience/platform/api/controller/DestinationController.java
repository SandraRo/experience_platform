package experience.platform.api.controller;

import experience.platform.db.model.Destination;
import experience.platform.db.model.Local;
import experience.platform.db.model.Tour;
import experience.platform.service.DestinationService;
import experience.platform.service.LocalService;
import experience.platform.service.TourService;
import experience.platform.util.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class DestinationController {

    private final DestinationService destinationService;
    private final LocalService localService;
    private final TourService tourService;

    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    public DestinationController(DestinationService destinationService, LocalService localService, TourService tourService) {
        this.destinationService = destinationService;
        this.localService = localService;
        this.tourService = tourService;
    }

    @PostMapping("/destination/create/{localId}")
    public String destinationCreate(Model model, @ModelAttribute("destination") Destination destination, @PathVariable int localId, RedirectAttributes redirectAttributes) {
        Local local = localService.findById(localId);
        redirectAttributes.addFlashAttribute("local", local);
        List<Tour> tours = tourService.findByLocal(local);
        redirectAttributes.addFlashAttribute("tours", tours);
        redirectAttributes.addFlashAttribute("newTour", new Tour());
        redirectAttributes.addFlashAttribute("destination", new Destination());
        if(!Validator.isValidDestination(destination)){
            redirectAttributes.addFlashAttribute("error", "Destination name cannot be emty");
            return "redirect:/destination/create/result";
        }
        Destination dbDest = destinationService.findByName(destination.getName());
        if (dbDest == null) {
            destinationService.save(new Destination(destination.getName()));
            redirectAttributes.addFlashAttribute("success", "Destination successfully created");
        } else {
            redirectAttributes.addFlashAttribute("error", "Destination already exists");
        }
        return "redirect:/destination/create/result";
    }

    @GetMapping("/destination/create/result")
    public String destinationCreate(Model model) {
        return "local-home";
    }
}
