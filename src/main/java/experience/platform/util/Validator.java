package experience.platform.util;

import experience.platform.api.model.ExperienceCreationRequest;
import experience.platform.api.model.LocalRegistrationRequest;
import experience.platform.db.model.Destination;
import experience.platform.db.model.Tour;
import experience.platform.db.model.Tourist;
import org.apache.logging.log4j.util.Strings;

public class Validator {

    public static boolean isValidDestination(Destination destination) {
        return destination != null && Strings.isNotBlank(destination.getName());
    }

    public static boolean isValidTour(Tour tour) {
        return tour != null && Strings.isNotBlank(tour.getName());
    }

    public static boolean isValidExperience(ExperienceCreationRequest experience) {
        return experience != null && Strings.isNotBlank(experience.getName()) && Strings.isNotBlank(experience.getPrice())
                && Strings.isNotBlank(experience.getDestination()) && Strings.isNotBlank(experience.getDescription());
    }

    public static boolean isValidLocal(LocalRegistrationRequest local) {
        return local != null && Strings.isNotBlank(local.getName()) && Strings.isNotBlank(local.getSurname())
                && Strings.isNotBlank(local.getUsername()) && Strings.isNotBlank(local.getPassword()) && Strings.isNotBlank(local.getDestination());
    }

    public static boolean isValidTourist(Tourist tourist) {
        return tourist != null && Strings.isNotBlank(tourist.getName()) && Strings.isNotBlank(tourist.getSurname())
                && Strings.isNotBlank(tourist.getUsername()) && Strings.isNotBlank(tourist.getPassword());
    }
}
